require 'net/http'
require "uri"
require 'json'


class ApiClient

  HOST = "http://cs5500-skynet.herokuapp.com/application"

  # Get all listings
  def get_listings
    url = "#{HOST}/listing"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)

    puts "Response code: #{response.code}"
    puts "Response body: #{response.body}"
 
    result = JSON.parse(response.body)
    return result
  end


 # Create a new listing
  def create_listing

   json_data = <<EOD
    
{
    "id": 0,
    "listingDetails": {
      "address": {
        "id": 0,
        "country": "usa",
        "state": "wa",
        "city": "seattle",
        "zipCode": 98109,
        "streetAddress": "E Madison St"
      },
      "description": "studio",
      "rent": 500,
      "utilities": [
        "TV"
      ]
    },
    "rating": 4,
    "unavailableDates": [      
        "2016-12-20"
      ],
    "ownerId": 72
  }
EOD
    url = "#{HOST}/listing/"
    uri = URI.parse(url)
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json'})
    req.body = json_data    
    req.basic_auth 'user', 'password'
    
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end
 
  #Get listings by username
  def get_listings_by_userName(uname)
    url = "#{HOST}/listing/username/#{uname}/"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)

    puts "Response code: #{response.code}"
    puts "Response body: #{response.body}"
 
    result = JSON.parse(response.body)
    return result
  end

  #Get listings by id
  def get_listings_by_id(id)
    url = "#{HOST}/listing/#{id}/"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)

    puts "Response code: #{response.code}"
    puts "Response body: #{response.body}"
 
    result = JSON.parse(response.body)
    return result
  end

  #Get listings by search criteria
  def get_listings_by_criteria(city, min, max, minR, maxR, startD, endD)
    url = "#{HOST}/listing/search?city=#{city}&minPrice=#{min}&maxPrice=#{max}&startDate=#{startD}&endDate=#{endD}&minRating=#{minR}&maxRating=#{maxR}"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)

    puts "Response code: #{response.code}"
    puts "Response body: #{response.body}"
 
    result = JSON.parse(response.body)
    return result
  end

 #Update an existing listing
 def update_listing()
    url = "#{HOST}/listing/"
    uri = URI.parse(url)

  json_data = <<EOD

	{
	  "id": 532,
	  "listingDetails": {
	    "address": {
	      "id": 0,
	      "country": "brazil",
	      "state": "wa",
	      "city": "rio",
	      "zipCode": 0,
	      "streetAddress": "string"
	    },
	    "description": "1BR",
	    "rent": 0,
	    "utilities": [
	      "TV"
	    ]
	  },
	  "rating": 0,
	  "unavailableDates": [
	    "2016-04-20"
	  ],
	  "ownerId": 72
	}

EOD

    req = Net::HTTP::Put.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = json_data
    req.basic_auth 'user', 'password'
    
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end

 # Delete an existing listing by its id
 def delete_listing(id)
    url = "#{HOST}/listing/#{id}/"
    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)
    req = Net::HTTP::Delete.new(uri.path)

    req.basic_auth 'user', 'password'
    res = http.request(req)
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end
end

