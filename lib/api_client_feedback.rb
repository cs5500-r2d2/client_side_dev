#CherylRuo
require 'net/http'
require "uri"
require 'json'


class ApiClient

  HOST = "http://cs5500-skynet.herokuapp.com/application"

  def get_feedback
    url = "#{HOST}/feedback"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)
 
    result = JSON.parse(response.body)
    puts result
    return result
  end
  
  def get_feedback_by_id(id)
      url = "#{HOST}/feedback/#{id}"
      uri = URI.parse(url)
      
      # Shortcut
      response = Net::HTTP.get_response(uri)
      
      result = JSON.parse(response.body)
      puts result
      return result
  end
  
  def create_feedback
      uri = URI('http://cs5500-skynet.herokuapp.com/application/feedback')
      req = Net::HTTP::Post.new(uri, initheader = {'Content-Type' =>'application/json'})
      req.body = {
          rating: 4,
          text: "feedback",
          userId: 231,
          listingId: 123}.to_json
      req.basic_auth 'bilal', 'password'
      
      res = Net::HTTP.start(uri.hostname, uri.port) do |http|
          http.request(req)
      end
      puts "Response code: #{res.code}"
      puts "Response body: #{res.body}"
      result = JSON.parse(res.body)
      return result
  end
  
  def delete_feedback(id)
      uri = URI("http://cs5500-skynet.herokuapp.com/application/feedback/#{id}")
      http = Net::HTTP.new(uri.host, uri.port)
      req = Net::HTTP::Delete.new(uri.path)
      req.basic_auth 'bilal', 'password'
      res = http.request(req)
      puts "Response code: #{res.code}"
      puts "Response body: #{res.body}"
      result = JSON.parse(res.body)
      return result
  end

  def update_feedback(rid)
      uri = URI('http://cs5500-skynet.herokuapp.com/application/feedback')
      req = Net::HTTP::Put.new(uri, initheader = {'Content-Type' =>'application/json'})
      req.body = {
          id: rid,
          rating: 3,
          text: "feedback",
          userId: 21,
          listingId: 13}.to_json
      req.basic_auth 'bilal', 'password'
    
      res = Net::HTTP.start(uri.hostname, uri.port) do |http|
          http.request(req)
      end
      puts "Response code: #{res.code}"
      puts "Response body: #{res.body}"
      result = JSON.parse(res.body)
      return result
  end
#  def get_feedback_by_listing_id(id)
#      url = "#{HOST}/feedback/listing/#{id}"
#      uri = URI.parse(url)
#      
#      # Shortcut
#      response = Net::HTTP.get_response(uri)
#      
#      result = JSON.parse(response.body)
#      puts result
#      return result
#  end
end
