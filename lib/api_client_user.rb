require 'net/http'
require "uri"
require 'json'


class ApiClient

  HOST = "http://cs5500-skynet.herokuapp.com/application"

  def get_users
    uri = URI('http://cs5500-skynet.herokuapp.com/application/user')
	req = Net::HTTP::Get.new(uri, initheader = {'Content-Type' =>'application/json'})
	req.basic_auth 'bilal', 'password'
  
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end

  def create_user
    uri = URI('http://cs5500-skynet.herokuapp.com/application/user')
    req = Net::HTTP::Post.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = {
	  role: "CLIENT",
	  userName: "test",
	  password: "string",
	  firstName: "string",
	  lastName: "string",
	  email: "string",
	  dateOfBirth: "2016-04-20",
	  cellPhone: "string"}.to_json
    req.basic_auth 'bilal', 'password'
    
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end
  
  def update_user(uid)
    uri = URI('http://cs5500-skynet.herokuapp.com/application/user')
    req = Net::HTTP::Put.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = {
      id: uid,
	  role: "ADMIN",
	  userName: "test2",
	  password: "string",
	  firstName: "string",
	  lastName: "string",
	  email: "string",
	  dateOfBirth: "2016-04-20",
	  cellPhone: "string"}.to_json
    req.basic_auth 'bilal', 'password'
    
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end
  
  def get_user_by_username(username)
	uri = URI("http://cs5500-skynet.herokuapp.com/application/user/username/#{username}")
    req = Net::HTTP::Get.new(uri, initheader = {'Content-Type' =>'application/json'})
	req.basic_auth 'bilal', 'password'
	
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    result = JSON.parse(res.body)
    return result
  end
  
  
  def get_user_by_id(id)
	uri = URI("http://cs5500-skynet.herokuapp.com/application/user/#{id}")
    req = Net::HTTP::Get.new(uri, initheader = {'Content-Type' =>'application/json'})
	req.basic_auth 'bilal', 'password'
	
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    result = JSON.parse(res.body)
    return result
  end
  
  def delete_by_id(id)
    uri = URI("http://cs5500-skynet.herokuapp.com/application/user/#{id}")
    http = Net::HTTP.new(uri.host, uri.port)
    req = Net::HTTP::Delete.new(uri.path)
    req.basic_auth 'bilal', 'password'
    res = http.request(req)
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end
  
end
