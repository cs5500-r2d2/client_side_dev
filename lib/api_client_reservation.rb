#Author: Bolun Hu
require 'net/http'
require "uri"
require 'json'


class ApiClient

  HOST = "http://cs5500-skynet.herokuapp.com/application"

  def get_reservations
    url = "#{HOST}/reservation"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)
 
    result = JSON.parse(response.body)
    return result
  end

  def create_reservation
    uri = URI('http://cs5500-skynet.herokuapp.com/application/reservation')
    req = Net::HTTP::Post.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = {
      ownerId: 72, 
      renterId: 52, 
      listingId: 162, 
      start: "2016-12-20", 
      end: "2016-12-20", 
      status: "PENDING"}.to_json
    req.basic_auth 'bilal', 'password'
    
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end

  def delete_reservation(id)
    uri = URI("http://cs5500-skynet.herokuapp.com/application/reservation/#{id}")
    http = Net::HTTP.new(uri.host, uri.port)
    req = Net::HTTP::Delete.new(uri.path)
    req.basic_auth 'bilal', 'password'
    res = http.request(req)
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end

  def update_reservation(rid)
    uri = URI('http://cs5500-skynet.herokuapp.com/application/reservation')
    req = Net::HTTP::Put.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = {
      id: rid,
      ownerId: 62, 
      renterId: 72, 
      listingId: 62, 
      start: "2016-09-30", 
      end: "2016-09-30", 
      status: "ACCEPTED"}.to_json
    req.basic_auth 'bilal', 'password'
    
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    puts "Response code: #{res.code}"
    puts "Response body: #{res.body}"
    result = JSON.parse(res.body)
    return result
  end

  def get_reservations_by_listing_id(id)
    url = "#{HOST}/reservation/listing/#{id}"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)
 
    result = JSON.parse(response.body)
    # puts result
    return result
  end

  def reject_reservation_by_id(id)
    uri = URI("http://cs5500-skynet.herokuapp.com/application/reservation/reject/#{id}")
    req = Net::HTTP::Put.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.basic_auth 'bilal', 'password'
    
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
    result = JSON.parse(res.body)
    return result
  end

  def get_rental_history_for_user(id)
    url = "#{HOST}/reservation/rentalHistory/#{id}"
    uri = URI.parse(url)

    req = Net::HTTP::Get.new(uri)
    req.basic_auth 'bilal', 'password'

    res = Net::HTTP.start(uri.hostname, uri.port) {|http|
      http.request(req)
    }
 
    result = JSON.parse(res.body)
    return result
  end

  def get_upcoming_rental_for_user(id)
    url = "#{HOST}/reservation/upcomingRental/#{id}"
    uri = URI.parse(url)

    req = Net::HTTP::Get.new(uri)
    req.basic_auth 'bilal', 'password'

    res = Net::HTTP.start(uri.hostname, uri.port) {|http|
      http.request(req)
    }
 
    result = JSON.parse(res.body)
    return result
  end

  def get_reservation_by_id(id)
    url = "#{HOST}/reservation/#{id}"
    uri = URI.parse(url)
  
    # Shortcut
    response = Net::HTTP.get_response(uri)
 
    result = JSON.parse(response.body)
    return result
  end
  
end
