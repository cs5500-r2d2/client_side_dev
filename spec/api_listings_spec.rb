require 'spec_helper'
 
describe ApiClient do
tempId = -1

  it "should get a 200 while fetching the all listings" do 
    client = ApiClient.new
    body = client.get_listings
    numberOfListings = body.length
    puts body
    expect(body[0]["id"]).to eql 162
  end

 it "should get a 200 while posting a new listing" do 
    client = ApiClient.new
    body = client.create_listing
    tempId = body["id"]
    puts body
    expect(body["listingDetails"]["address"]["city"]).to eql "seattle"
  end 

 it "should get a 200 while fetching the listings through criteria" do 
    client = ApiClient.new
    body = client.get_listings_by_criteria('seattle', '200', '500', '0', '4', 1460680933000, 1461976933000)
    puts body
    expect(body[0]["listingDetails"]["address"]["city"]).to eql "seattle"
  end

it "should get a 200 while fetching the specified listing through username" do 
    client = ApiClient.new
    body = client.get_listings_by_userName("user")
    puts body
    expect(body[0]["ownerId"]).to eql 72

 end

it "should get a 200 while fetching the specified listing through id" do 
    client = ApiClient.new
    body = client.get_listings_by_id(tempId)
    puts body
    expect(body["ownerId"]).to eql 72

 end

it "should get a 200 while updating the specified listing" do 
    client = ApiClient.new
    body = client.update_listing()
    puts body
    expect(body["listingDetails"]["address"]["city"]).to eql 'rio'
end

it "should get a 200 while deleting the specified listing by id" do 
    client = ApiClient.new
    body = client.delete_listing(tempId)
    puts body
    expect(body["id"]).to eql tempId
end
end


