require 'spec_helper'
 
describe ApiClient do

  it "should get all the users" do 
    client = ApiClient.new
    body = client.get_users
    puts body
	expect(body[0]["id"]).to eql 52
    expect(body[0]["role"]).to eql "ADMIN"
    expect(body[0]["userName"]).to eql "bilal"
    expect(body[0]["password"]).to eql "password"
	expect(body[0]["firstName"]).to eql "string"
	expect(body[0]["lastName"]).to eql "string"
	expect(body[0]["email"]).to eql "string"
    expect(body[0]["dateOfBirth"][0]).to eql 2016
    expect(body[0]["dateOfBirth"][1]).to eql 3
    expect(body[0]["dateOfBirth"][2]).to eql 31
    expect(body[0]["cellPhone"]).to eql "string"
  end

  toRemove = -1  

  it "should create a new user" do
    client = ApiClient.new
    body = client.create_user
    puts body
    expect(body["userName"]).to eql "test"

    toRemove = body["id"]
  end
  
  it "should update a user" do
    client = ApiClient.new
    body = client.update_user(92);
    puts body
    expect(body["userName"]).to eql "test2"
  end
  
  it "should retrieve a user by username" do 
    client = ApiClient.new
    body = client.get_user_by_username("bilal")
    puts body
    expect(body["userName"]).to eql "bilal"
  end
  
  it "should delete a user by id" do
    client = ApiClient.new
    body = client.delete_by_id(toRemove)
    puts body
    expect(body["id"]).to eql toRemove
  end
  
  it "should retrieve a user by id" do
    client = ApiClient.new
    body = client.get_user_by_id(92)
    puts body
    expect(body["id"]).to eql 92
  end

end
