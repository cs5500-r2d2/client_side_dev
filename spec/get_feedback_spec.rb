# CherylRuo
require 'spec_helper'

describe ApiClient do
    
    it "should get a list of feedback when fetching the homepage" do
        client = ApiClient.new
        body = client.get_feedback
        puts body
        expect(body[0]["id"]).to eql 52
        expect(body[0]["rating"]).to eql 3
        expect(body[0]["text"]).to eql "feedback"
        expect(body[0]["userId"]).to eql 21
        expect(body[0]["listingId"]).to eql 13
    end
    
    toRemove = -1
    
    it "should create a new feedback into the system" do
        client = ApiClient.new
        body = client.create_feedback
        puts body
        expect(body["rating"]).to eql 4
        expect(body["text"]).to eql "feedback"
        expect(body["userId"]).to eql 231
        expect(body["listingId"]).to eql 123
        
        toRemove = body["id"]
    end
    
    it "should delete a feedback with specified id" do
        client = ApiClient.new
        body = client.delete_feedback(toRemove)
        puts body
        expect(body["id"]).to eql toRemove
    end
    
    it "should update a feedback with specified id" do
        client = ApiClient.new
        body = client.update_feedback(52);
        puts body
        expect(body["id"]).to eql 52
    end
    
#    it "should get a list of feedback for a specified listing id" do
#        client = ApiClient.new
#        body = client.get_feedback_by_listing_id(123)
#        puts body
#        expect(body["rating"]).to eql 4
#        expect(body["text"]).to eql "feedback"
#        expect(body["userId"]).to eql 231
#        expect(body["listingId"]).to eql 123
#    end

    it "should get a feedback by specified id" do
        client = ApiClient.new
        body = client.get_feedback_by_id(52)
        puts body
        expect(body["id"]).to eql 52
    end
end
