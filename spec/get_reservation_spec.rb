#Author: Bolun Hu
require 'spec_helper'
 
describe ApiClient do

  it "should get a list of reservations when fetching the homepage" do 
    client = ApiClient.new
    body = client.get_reservations
    puts body
    expect(body[0]["id"]).to eql 152
    expect(body[0]["ownerId"]).to eql 62
    expect(body[0]["renterId"]).to eql 72
    expect(body[0]["listingId"]).to eql 62
    expect(body[0]["start"][0]).to eql 2016
    expect(body[0]["start"][1]).to eql 9
    expect(body[0]["start"][2]).to eql 30
    expect(body[0]["end"][0]).to eql 2016
    expect(body[0]["end"][1]).to eql 9
    expect(body[0]["end"][2]).to eql 30
    expect(body[0]["status"]).to eql "ACCEPTED"
  end 

  toRemove = -1

  it "should create a new reservation into the system" do
    client = ApiClient.new
    body = client.create_reservation
    puts body
    expect(body["ownerId"]).to eql 72

    toRemove = body["id"]
  end

  it "should delete a reservation with specified id" do
    client = ApiClient.new
    body = client.delete_reservation(toRemove)
    puts body
    expect(body["id"]).to eql toRemove
  end

  it "should update a reservation with specified id" do
    client = ApiClient.new
    body = client.update_reservation(152);
    puts body
    expect(body["id"]).to eql 152
  end

  it "should reject a reservation by id" do
    client = ApiClient.new
    body = client.reject_reservation_by_id(212)
    puts body
    expect(body["id"]).to eql 212
    expect(body["status"]).to eql "REJECTED"
  end

  it "should get a list of reservations for a specified listing id" do
    client = ApiClient.new
    body = client.get_reservations_by_listing_id(162)
    puts body
    expect(body[0]["id"]).to eql 202
    expect(body[0]["ownerId"]).to eql 52
    expect(body[0]["renterId"]).to eql 72
    expect(body[0]["listingId"]).to eql 162
    expect(body[0]["start"][0]).to eql 2016
    expect(body[0]["start"][1]).to eql 10
    expect(body[0]["start"][2]).to eql 15
    expect(body[0]["end"][0]).to eql 2016
    expect(body[0]["end"][1]).to eql 10
    expect(body[0]["end"][2]).to eql 15
    expect(body[0]["status"]).to eql "ACCEPTED"

    expect(body[1]["id"]).to eql 212
    expect(body[1]["ownerId"]).to eql 52
    expect(body[1]["renterId"]).to eql 62
    expect(body[1]["listingId"]).to eql 162
    expect(body[1]["start"][0]).to eql 2016
    expect(body[1]["start"][1]).to eql 11
    expect(body[1]["start"][2]).to eql 15
    expect(body[1]["end"][0]).to eql 2016
    expect(body[1]["end"][1]).to eql 11
    expect(body[1]["end"][2]).to eql 15
    expect(body[1]["status"]).to eql "REJECTED"
  end

  it "should get a list of rental history for a specified user" do
    client = ApiClient.new
    body = client.get_rental_history_for_user(52)
    puts body
    expect(body[0]["id"]).to eql 282
    expect(body[0]["ownerId"]).to eql 72
    expect(body[0]["renterId"]).to eql 52
    expect(body[0]["listingId"]).to eql 152
    expect(body[0]["start"][0]).to eql 2016
    expect(body[0]["start"][1]).to eql 2
    expect(body[0]["start"][2]).to eql 14
    expect(body[0]["end"][0]).to eql 2016
    expect(body[0]["end"][1]).to eql 2
    expect(body[0]["end"][2]).to eql 14
    expect(body[0]["status"]).to eql "ACCEPTED"
  end

  it "should get a list of upcoming rentals for a specified user" do
    client = ApiClient.new
    body = client.get_upcoming_rental_for_user(52)
    puts body
    expect(body[0]["id"]).to eql 162
    expect(body[0]["ownerId"]).to eql 72
    expect(body[0]["renterId"]).to eql 52
    expect(body[0]["listingId"]).to eql 152
    expect(body[0]["start"][0]).to eql 2016
    expect(body[0]["start"][1]).to eql 11
    expect(body[0]["start"][2]).to eql 20
    expect(body[0]["end"][0]).to eql 2016
    expect(body[0]["end"][1]).to eql 11
    expect(body[0]["end"][2]).to eql 20
    expect(body[0]["status"]).to eql "ACCEPTED"

    expect(body[1]["id"]).to eql 242
    expect(body[1]["ownerId"]).to eql 72
    expect(body[1]["renterId"]).to eql 52
    expect(body[1]["listingId"]).to eql 152
    expect(body[1]["start"][0]).to eql 2016
    expect(body[1]["start"][1]).to eql 7
    expect(body[1]["start"][2]).to eql 15
    expect(body[1]["end"][0]).to eql 2016
    expect(body[1]["end"][1]).to eql 7
    expect(body[1]["end"][2]).to eql 15
    expect(body[1]["status"]).to eql "ACCEPTED"
  end

  it "should get a reservation by specified id" do
    client = ApiClient.new
    body = client.get_reservation_by_id(152)
    puts body
    expect(body["id"]).to eql 152
  end

end
